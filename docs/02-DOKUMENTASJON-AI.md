[Egenerklæring](./01-EGENERKLÆRING.md) &bullet; [README](../README.md) &bullet; [Generell informasjon](./03-INFO.md)
# Dokumentasjon ved bruk av AI


Dokumentasjon ved bruk av AI
Dersom du har benyttet kunstig intelligens (f. eks. ChatGPT) under noen del av besvarelsen av eksamen, må du dokumentere dette ved å laste opp skjermbilder som viser både dine spørsmål og svarene fra AI'en. Dette kan lastes opp i punkt 6 her på vurdering.uib.no. Unntak ved bruk av CoPilot, dette trenger ikke dokumenteres med skjermbilde (men større avsnitt må fremdeles siteres i kildekoden). Alle avsnitt på tre eller flere sammenhengene kodelinjer som kommer fra AI skal siteres.


**Å unnlate å sitere kilder, herunder støtte fra AI, vil ansees som juks.**

