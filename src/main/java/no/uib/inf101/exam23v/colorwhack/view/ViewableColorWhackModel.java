package no.uib.inf101.exam23v.colorwhack.view;

import no.uib.inf101.exam23v.colorwhack.model.GameState;
import no.uib.inf101.grid.IReadOnlyGrid;

import java.awt.Color;
import java.time.LocalDateTime;

/** A viewable (read-only) model for a ColorWhack game. */
public interface ViewableColorWhackModel {

  /** Gets the current board. */
  IReadOnlyGrid<Color> getBoard();

  /** Gets the current score. */
  int getScorePercentage();

  /** Gets the time remaining in seconds. */
  int getTimeRemaining(LocalDateTime now);

  /** Gets the current game state. */
  GameState getGameState();

}
