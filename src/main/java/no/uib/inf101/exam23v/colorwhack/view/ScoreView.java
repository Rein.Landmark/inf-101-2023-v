package no.uib.inf101.exam23v.colorwhack.view;

import no.uib.inf101.exam23v.colorwhack.model.GameState;
import no.uib.inf101.graphics.Inf101Graphics;

import javax.swing.JPanel;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.time.LocalDateTime;
import java.util.Objects;

/**
 * A view for the score of a ColorWhack game. This display shows the
 * time remaining and the score percentage,or a welcome message if
 * the game has not started yet.
 */
public class ScoreView extends JPanel {

  private static final String WAITING_MESSAGE = "Keep it black!";
  private static final String GAME_OVER_MESSAGE = "Time is out!";

  private final ViewableColorWhackModel model;

  /**
   * Creates a new score view.
   *
   * @param model the model to display (non-null)
   */
  public ScoreView(ViewableColorWhackModel model) {
    Objects.requireNonNull(model);

    this.model = model;
    this.setPreferredSize(new Dimension(100, 30));
  }

  @Override
  protected void paintComponent(Graphics g) {
    super.paintComponent(g);
    Graphics2D g2 = (Graphics2D)g;
    switch (this.model.getGameState()) {
      case WAITING -> paintWaitingMessage(g2);
      case RUNNING, GAME_OVER -> paintRunningScore(g2);
    }
  }

  private void paintWaitingMessage(Graphics2D g) {
    double cx = this.getWidth() / 2.0;
    double cy = this.getHeight() / 2.0;
    Inf101Graphics.drawCenteredString(g, WAITING_MESSAGE, cx, cy);
  }

  private void paintRunningScore(Graphics2D g) {
    double halfWidth = this.getWidth() / 2.0;
    String leftText = Objects.equals(this.model.getGameState(), GameState.GAME_OVER)
        ? GAME_OVER_MESSAGE
        : String.valueOf(this.model.getTimeRemaining(LocalDateTime.now()));
    String rightText = this.model.getScorePercentage() + "%";
    Inf101Graphics.drawCenteredString(g, leftText, 0, 0, halfWidth, this.getHeight());
    Inf101Graphics.drawCenteredString(g, rightText, halfWidth, 0, halfWidth, this.getHeight());
  }
}
